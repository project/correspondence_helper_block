<?php
    // snippet: https://stefvanlooveren.me/node/74
    namespace Drupal\correspondence_helper_block\Form;
    use Drupal\Core\Form\ConfigFormBase;
    use Drupal\Core\Form\FormStateInterface;

    /**
     * Class AdminSettingsForm.
     *
     * @package Drupal\correspondence_helper_block\Form
     */
    class AdminSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'correspondence_helper_block.settings';

      /**
       * {@inheritdoc}
       */
      public function getFormId() {
        return 'correspondence_helper_block_admin_settings';
      }

      /**
       * {@inheritdoc}
       */
      protected function getEditableConfigNames() {
        return [
          static::SETTINGS,
        ];
      }

      /**
       * {@inheritdoc}
       */
      public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config(static::SETTINGS);

  $form['communication_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Communication Text Message. This text is displayed before the confirmation email on file.'),
      '#default_value' => $config->get('communication_text'),
    ];  

    $form['support_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Support Contact Text. This text holds support team  contact information.'),
      '#default_value' => $config->get('support_text'),
    ];

        return parent::buildForm($form, $form_state);
      }

 /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('communication_text', $form_state->getValue('communication_text'))
      // You can set multiple configurations at once by making
      // multiple calls to set().
      ->set('support_text', $form_state->getValue('support_text'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
