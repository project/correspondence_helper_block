<?php
namespace Drupal\correspondence_helper_block\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a block with some simple text
 *
 * @Block(
 *   id = "correspondence_helper_block",
 *   admin_label = @Translation("Correspondence Helper Block"),
 * )
 */

class CorrespondenceHelperBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */

  public function build() {
   $userCurrent = \Drupal::currentUser();
   $user = \Drupal\user\Entity\User::load($userCurrent->id());
   $mail= $user->get('mail')->value;
   $config = \Drupal::config('correspondence_helper_block.settings');
   $communication_text =  $config->get('communication_text');
   $support_text = $config->get('support_text');
    return [
      '#markup' => $this->t($communication_text.'<br><b>'.$mail.'</b><br><br>'.$support_text),
    ];

   }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['correspondence_helper_block_settings'] = $form_state->getValue('correspondence_helper_block_settings');
  }
}
